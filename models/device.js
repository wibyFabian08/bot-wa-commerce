"use strict";

module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "Device",
    // Fields
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      partner_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      phone_number: DataTypes.STRING,
      status: DataTypes.ENUM("pending", "active", "expire", "timeout"),
      expire_date: DataTypes.DATE,
      apikey: DataTypes.STRING,
      notif_alert: DataTypes.STRING,
      session: DataTypes.TEXT,
      qrcode: DataTypes.TEXT,
      paid_status: DataTypes.ENUM("pending", "paid", "expired", "blocked"),
    },
    // Model options
    {
      tableName: "device",
      timestamps: false,
    }
  );
};
